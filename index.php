<!DOCTYPE html>
<html>
<head>
	<title>Farid</title>
	<!-- menghubungkan dengan file css -->
	<link rel="stylesheet" type="text/css" href="style.css">
	<!-- menghubungkan dengan file jquery -->
	<script type="text/javascript" src="jquery.js"></script>
	   <!-- Required meta tags -->
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300" />

	<!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
</head>
<body>

<div class="content">
	<!-- form -->

    <div class="countainer">
      <div class="row">
        <div class="col-md-7 py-5">
        <div class="col-6 col-md-3">
          <a href="index.php?page=home"><img class="w-100" src="https://91.233.198.11/_aps4497/assets/images/logo-rgocasino.png" alt="Dingdong" /></a>
        </div>

        </div>
        <div class="col-md-5 py-4">
          <form>
            <div class="row">
              <div class="col">
                <input type="text" class="form-control" placeholder="Username" aria-label="Username" />
              </div>
              <div class="col">
                <input type="text" class="form-control" placeholder="Password" aria-label="Password" />
              </div>
              <div class="col">
                <button type="submit" class="btn btn-primary col-md-10">Sign In</button>
              </div>
            </div>

            <div class="mb-3"></div>

            <div class="row">
              <div class="col">
                <input type="text" class="form-control" placeholder="Captcha" aria-label="Captcha" />
              </div>
              <div class="col">
                <div class="row">
                  <div class="col">
                    <img src="https://91.233.198.11/user/captcha_get/55/23/4/sess_code_user_login/354" class="img-fluid " />
                  </div>
                  <div class="col">
                    <a href="bla.php">Lupa Password?</a>
                  </div>
                </div>
              </div>
              <div class="col">
                <button type="submit" class="btn btn-primary col-md-10 bg-danger">Register</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- akhir form -->

	 <!-- navbar -->
	 <div class="container" id="menu-nav">
      <div id="navigation-bar">
		<ul>
			<li>
            <a href="index.php?page=home"><span>Home</span></a>
          </li>
          <li>
            <a href="index.php?page=games"><span>Games</span></a>
          </li>
          <li>
            <a href="index.php?page=daftar"><span>Daftar</span></a>
          </li>
          <li>
            <a href="index.php?page=panduan"><span>Panduan</span></a>
          </li>
          <li>
            <a href="index.php?page=referal"> <span>Referal</span></a>
          </li>
          <li>
            <a href="index.php?page=promo"> <span>Promo</span></a>
          </li>
          <li>
            <a href="index.php?page=nawala"> <span>Nawala</span></a>
          </li>
		</ul>
	</div>
	<!-- akhir navbar -->

	<div class="badan">


	<?php 
	if(isset($_GET['page'])){
		$page = $_GET['page'];

		switch ($page) {
			case 'home':
				include "halaman/home.php";
				break;
			case 'games':
				include "halaman/games.php";
				break;
			case 'daftar':
				include "halaman/daftar.php";
				break;	
      case 'dingdong':
				include "halaman/dingdong.php";
				break;  
      case 'card':
				include "halaman/card.php";
				break; 		
			default:
				echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
				break;
		}
	}else{
		include "halaman/home.php";
	}

	 ?>

	</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>