
<div class="halaman" style="background-color: #fff">
<div class="border">
    <h1>Registrasi</h1>
</div>
<div class="container">
        <div class="row">
            <div class="col-md-3 border">
				<div class="py-3" >
                    <img src="https://91.233.198.11/_aps4497/assets/images/1.png" class="img-fluid " />
                </div>
				<div class="py-3" >
					<a style="color: #c2c2c2" >Data Login</a>
                </div>
				<form>
				<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">Username</label>
					<input type="username" class="form-control" id="exampleInputUsername" aria-describedby="usernameHelp">
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword1" class="form-label">Password</label>
					<input type="password1" class="form-control" id="exampleInputPassword1">
				</div>
				<div class="mb-3">
					<label for="exampleInputPassword2" class="form-label">Konfirmasi Password</label>
					<input type="password2" class="form-control" id="exampleInputPassword2">
				</div>
	
				</form> 
			</div>


            <div class="col-md-3 border">
            	<div class="py-3" >
                    <img src="https://91.233.198.11/_aps4497/assets/images/2.png" class="img-fluid " />
                </div>
				<div class="py-3" >
					<a style="color: #c2c2c2" >Data Pribadi</a>
                </div>
				<form>
				<div class="mb-3">
					<label for="exampleInputEmail1" class="form-label">E-mail</label>
					<input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp">
				</div>
				<div class="mb-3">
					<label for="exampleInputTelepon" class="form-label">Telepon</label>
					<input type="telepon" class="form-control" id="exampleInputTelepon">
				</div>
				<div class="mb-3">
					<label for="exampleInputReferral" class="form-label">Referral</label>
					<input type="referral" class="form-control" id="exampleInputReferral">
				</div>
				</form>    
			</div>


            <div class="col-md-3 border">
				<div class="py-3" >
                    <img src="https://91.233.198.11/_aps4497/assets/images/3.png" class="img-fluid " />
                </div>
				<div class="py-3" >
					<a style="color: #c2c2c2" >Data Bank</a>
                </div>
				<div class="mb-3 py-2">
					<div class="form-floating">
							<select class="form-select" id="floatingSelect" aria-label="Floating label select example">
								<option selected>--Pilih Bank--</option>
								<option value="1">BNI</option>
								<option value="2">BCA</option>
								<option value="3">BRI</option>
							</select>
							
					</div>
				</div>
				<form>
					<div class="mb-3">
					<label for="exampleInputNamaBank" class="form-label">Nama Rekening Bank</label>
					<input type="namabank" class="form-control" id="exampleInputNamaBank">
				</div>
				<div class="mb-3">
					<label for="exampleInputNomorBank" class="form-label">Nomor Rekening Bank</label>
					<input type="nomorbank" class="form-control" id="exampleInputNomorBank">
				</div>
	
				</form>
			</div>
            <div class="col-md-3 border">
				<label for="exampleSelectPertanyaan" class="form-label">Pertanyaan Rahasia</label>
				<div class="mb-3 py-2">
					<div class="form-floating">
							<select class="form-select" id="floatingSelect" aria-label="Floating label select example">
								<option selected>--Pilih Pertanyaan--</option>
								<option value="1">Siapa nama ibumu?</option>
								<option value="2">Buah yang kamu sukai?</option>
								<option value="3">Nama peliharaanmu?</option>
							</select>
							
					</div>
				</div>
				<form>
					<div class="mb-3">
						<label	label for="exampleInputJawaban" class="form-label">Jawaban Pertanyaan</label>
						<input type="jawaban" class="form-control" id="exampleInputJawaban">
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col">
								<input type="text" class="form-control" placeholder="captcha" aria-label="Captcha" />
							</div>
							<div class="col">
							<img src="https://91.233.198.11/user/captcha_get/55/23/4/sess_code_user_login/354" class="img-fluid " />
							</div>
						</div>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-md-3">
							<img src="https://91.233.198.11/_aps4497/assets/images/check.png" class="img-fluid " />
							</div>
							<div class="col">
							<p class="fs-6">anda menyetujui semua ketentuan</p>
							</div>
						</div>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-md-3">
							<img src="https://91.233.198.11/_aps4497/assets/images/18.png" class="img-fluid " />
							</div>
							<div class="col">
							<p class="fs-6">anda 18+</p>
							</div>
						</div>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-md-3">
							<img src="https://91.233.198.11/_aps4497/assets/images/security.png" class="img-fluid " />
							</div>
							<div class="col">
							<p class="fs-6">Semua data anda aman</p>
							</div>
						</div>
					</div>


				</form>
				<div class="mb-3 py-2">
				<button type="submit" class="btn btn-primary col-md-10">Daftar</button>
				</div>

			</div>

        </div>
    </div>
</div>